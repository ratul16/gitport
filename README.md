# Simple Portfolio Website
hosted now : ratul16.bitbucket.io

## Getting Started

The website was built using HTML,CSS and JAVASCRIPT. You can easily run it on your own local machine.


### Installing

* Clone repo using (git clone https://github.com/ratul16/simpleportfolio.git)
* install vscode (https://code.visualstudio.com/)
* Open with vscode and start working

## Built With

* HTML - Used for web structure
* CSS - Stylesheets
* Javascript - Animation

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* **Sabbir Molla** - Helped with the Structure of the website. Profile : (https://github.com/SabbirMollah)

* Original Dev : **Mifta Sintaha**





